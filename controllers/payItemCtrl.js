const { PayItemModel } = require('../models');
const ObjectId = require("mongodb").ObjectId;

module.exports = {
  getAllPayItem: async (req, res, next) => {
    const params = req.body
    const payItems = await PayItemModel
      .find({}, 'user_id date_of_pay description')
      .limit(params.limit || 10)
      .skip(params.skip || 0) || [];
    const total = await PayItemModel.countDocuments({});
    if (payItems.length === 0) {
      req.status = 404;
      req.return = { message: "Have no PayItems !" };
      next();
    } else {
      req.status = 200;
      req.return = { message: "Success !", data: payItems, total, limit: params.limit || 10, skip: params.skip || 0 };
      next();
    }
  },
  getSinglePayItem: async (req, res, next) => {
    const params = req.body;
    const pay = await PayItemModel.find({ _id: ObjectId(params.pay_id) });
    if (pay.length === 0) {
      req.status = 404;
      req.return = { message: "Have no PayItem !" };
      next();
    }
    req.status = 200;
    req.return = { message: "Success !", data: pay };
    next();
  },

  getAllPayItemByUser: async (req, res, next) => {
    const params = req.body
    const payItems = await PayItemModel
      .find({ user_id: params.user_id })
      .limit(params.limit || 10)
      .skip(params.skip || 0) || [];
    const total = await PayItemModel.countDocuments({ user_id: params.user_id });
    if (payItems.length === 0) {
      req.status = 404;
      req.return = { message: "Have no PayItems !" };
      next();
    }
    req.status = 200;
    req.return = { message: "Success !", data: payItems, total, limit: params.limit || 10, skip: params.skip || 0 };
    next();
  },

  insertPayItem: async (req, res, next) => {
    const payItem = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined) {
        const payItemObject = new PayItemModel({
          ...payItem,
          date_of_pay: new Date()
        });
        const result = await payItemObject.save();
        req.status = 200;
        req.return = { message: "Create Success !", data: payItemObject, result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Create Failed!", error: err };
      next();
    }
  },
  updatePayItem: async (req, res, next) => {
    const updatePayItem = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const result = await PayItemModel.updateOne(
          { _id: ObjectId(updatePayItem.pay_id) },
          {
            description: updatePayItem.description,
            date_of_pay: updatePayItem.date_of_pay,
          },
          {
            useFindAndModify: false,
            new: true
          }
        );
        req.status = 200;
        req.return = { message: 'Updated Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Update Failed!", error: err };
      next();
    }
  },
  deletePayItem: async (req, res, next) => {
    const { pay_id } = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const result = await PayItemModel.remove({ _id: ObjectId(pay_id) });
        req.status = 200;
        req.return = { message: 'Delete Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Deleted Failed!", error: err };
      next();
    }
  }
}