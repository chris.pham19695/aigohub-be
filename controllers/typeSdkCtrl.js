const { TypeSDKModel } = require('../models');
const ObjectId = require("mongodb").ObjectId;

module.exports = {
  getAllTypeSDK: async (req, res, next) => {
    const typeSDK = await TypeSDKModel.find({});
    if (typeSDK.length === 0) {
      req.status = 404;
      req.return = { message: "Have no TypeSDK !" };
      next();
    } else {
      req.status = 200;
      req.return = { message: "Success !", data: typeSDK };
      next();
    }
  },
  insertTypeSDK: async (req, res, next) => {
    const typeSDK = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const sdk = new TypeSDKModel(typeSDK);
        const result = await sdk.save();
        req.status = 200;
        req.return = { message: "Insert Success !", data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Insert Failed!", error: err };
      next();
    }
  },
  updateTypeSDK: async (req, res, next) => {
    const typeSDK = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const result = await TypeSDKModel.updateOne(
          { _id: ObjectId(typeSDK.id) },
          {
            description: typeSDK.description
          },
          {
            useFindAndModify: false,
            new: true
          }
        );
        req.status = 200;
        req.return = { message: 'Updated Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Update Failed!", error: err };
      next();
    }
  },
  deleteTypeSDK: async (req, res, next) => {
    const typeSDK = req.body;
    const auth = req.auth;
    console.log(123, typeSDK)
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const result = await TypeSDKModel.remove({ _id: ObjectId(typeSDK.sdk_id) });
        req.status = 200;
        req.return = { message: 'Delete Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Deleted Failed!", error: err };
      next();
    }
  }
}