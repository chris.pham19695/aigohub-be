const { PostItemModel, UserModel, CategoryModel } = require('../models');
const ObjectId = require("mongodb").ObjectId;

module.exports = {
  getAllPost: async (req, res, next) => {
    const params = req.body
    const postItems = await PostItemModel
      .find(params.name ? { name: { '$regex': params.name.toLowerCase(), '$options': 'i' } }: {})
      .limit(params.limit || 10)
      .skip(params.skip || 0) || [];
    const total = await PostItemModel.countDocuments(params.name ? { name: { '$regex': params.name.toLowerCase(), '$options': 'i' } } : {});
    if (postItems.length === 0) {
      req.status = 404;
      req.return = { message: "Have no Posts !" };
      next();
    }
    req.status = 200;
    req.return = { message: "Success !", data: postItems, total, limit: params.limit || 10, skip: params.skip || 0 };
    next();
  },
  getSinglePost: async (req, res, next) => {
    const params = req.body;
    const post = await PostItemModel.find({ _id: ObjectId(params.id) });
    if (post.length === 0) {
      req.status = 404;
      req.return = { message: "Have no Post !" };
      next();
    }
    req.status = 200;
    req.return = { message: "Success !", data: post };
    next();
  },
  insertPostItem: async (req, res, next) => {
    const postItemObject = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const postItem = new PostItemModel(postItemObject);
        const result = await postItem.save();
        req.status = 200;
        req.return = { message: "Create Success !", data: postItemObject, result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Create Failed!", error: err };
      next();
    }
  },
  updatePost: async (req, res, next) => {
    const updatePost = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const result = await PostItemModel.updateOne(
          { _id: ObjectId(updatePost.id) },
          {
            name: updatePost.name,
            description: updatePost.description
          },
          {
            useFindAndModify: false,
            new: true
          }
        );
        req.status = 200;
        req.return = { message: 'Updated Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Update Failed!", error: err };
      next();
    }
  },
  deletePost: async (req, res, next) => {
    const { id } = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const result = await PostItemModel.remove({ _id: ObjectId(id) });
        req.status = 200;
        req.return = { message: 'Delete Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Deleted Failed!", error: err };
      next();
    }
  }
}