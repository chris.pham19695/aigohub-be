const { TypeSDK_UserModel, TypeSDKModel } = require('../models');
const shortid = require('../utils/short-id.service');
const ObjectId = require("mongodb").ObjectId;

module.exports = {
  getAllTypeSDKUser: async (req, res, next) => {
    const params = req.body;
    const typeSDKUser = await TypeSDK_UserModel
      .find({ user_id: ObjectId(params.user_id) })
      .limit(params.limit || 10)
      .skip(params.skip || 0)
    if (typeSDKUser.length === 0) {
      req.status = 404;
      req.return = { message: "Have no TypeSDKUser !" };
      next();
    } else {
      req.status = 200;
      req.return = { message: "Success !", data: typeSDKUser };
      next();
    }
  },
  handleTypeSdk: async (sdk_id) => {
    Date.prototype.addDays = function (days) {
      this.setDate(this.getDate() + parseInt(days));
      return this;
    };
    const typeSDK = await TypeSDKModel.find({ _id: ObjectId(sdk_id) });
    if (typeSDK.length === 0) {
      return 0;
    } else {
      if (typeSDK[0].description === 'TRIAL') return new Date().addDays(7);
      if (typeSDK[0].description === 'QUATER') return new Date().addDays(90);
      if (typeSDK[0].description === 'YEAR') return new Date().addDays(365);;
    }
  },
  insertTypeSDKUser: async (req, res, next) => {
    const typeSDK = req.body;
    const auth = req.auth;
    console.log(typeSDK)
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const sdk = new TypeSDK_UserModel({
          ...typeSDK,
          key: shortid.generate(),
          create_date: new Date(),
          expired_date: await module.exports.handleTypeSdk(typeSDK.sdk_id)
        });
        const result = await sdk.save();
        req.status = 200;
        req.return = { message: "Insert Success !", data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Insert Failed!", error: err };
      next();
    }
  },
  updateTypeSDKUser: async (req, res, next) => {
    const typeSDK = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const result = await TypeSDK_UserModel.updateOne(
          { _id: ObjectId(typeSDK.user_id) },
          {
            description: typeSDK.description,
            sdk_id: typeSDK.sdk_id,
            key: shortid.generate(),
            create_date: new Date(),
            expired_date: await module.exports.handleTypeSdk(typeSDK.sdk_id)
          },
          {
            useFindAndModify: false,
            new: true
          }
        );
        req.status = 200;
        req.return = { message: 'Updated Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Update Failed!", error: err };
      next();
    }
  },
  deleteTypeSDKUser: async (req, res, next) => {
    const typeSDK = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const result = await TypeSDK_UserModel.remove({ sdk_id: ObjectId(typeSDK.sdk_id), user_id: ObjectId(typeSDK.user_id) });
        req.status = 200;
        req.return = { message: 'Delete Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Deleted Failed!", error: err };
      next();
    }
  }
}