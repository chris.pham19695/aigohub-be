const { UserModel } = require('../models/');
const ObjectId = require("mongodb").ObjectId;
const AuthUtil = require('../utils/auth');
const { configAuth } = require('../configs/configAuth');;


module.exports = {
  getAllUser: async (req, res, next) => {
    const params = req.body
    try {
      const users = await UserModel
        .find(params.email ? { email: { '$regex': params.email.toLowerCase(), '$options': 'i' } } : {}, 'role email username phone')
        .limit(params.limit || 10)
        .skip(params.skip || 0) || [];
      let total = await UserModel.countDocuments(params.email ? { email: { '$regex': params.email.toLowerCase(), '$options': 'i' } } : {});
      if (users.length === 0) {
        req.status = 404;
        req.return = { message: "Have no Users !" };
        next();
      } else {
        req.status = 200;
        req.return = { message: "Success !", data: users, total, limit: params.limit || 10, skip: params.skip || 0 };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: err.message, error: err };
      next();
    }
  },
  getSingleUser: async (req, res, next) => {
    const params = req.body;
    try {
      const user = await UserModel.find({ _id: ObjectId(params.id) }, 'email phone username');
      if (user.length === 0) {
        req.status = 404;
        req.return = { message: "Have no User !" };
        next();
      }
      req.status = 200;
      req.return = { message: "Success !", data: user };
      next();
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: err.message, error: err };
      next();
    }
  },
  insertUser: async (req, res, next) => {
    const UserObject = req.body;
    const auth = req.auth;
    try {
      // if (auth !== undefined) {
      const user = new UserModel(UserObject);
      const result = await user.save();
      req.status = 200;
      req.return = { message: "Create Success !" };
      next();
      // } else {
      //   req.status = 403;
      //   req.return = { message: 'Permission Dennied' };
      //   next();
      // }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: err.message, error: err };
      next();
    }
  },
  deleteUser: async (req, res, next) => {
    const user = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const result = UserModel.remove({ _id: ObjectId(user.id) });
        req.status = 200;
        req.return = { message: 'Deleted Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: err.message, error: err };
      next();
    }
  },
  updateUser: async (req, res, next) => {
    const user = req.body;
    const auth = req.auth;
    try {
      if (auth !== 'undefined') {
        const result = await UserModel.updateOne(
          { _id: ObjectId(user.id) },
          {
            email: user.email,
            password: user.password,
            username: user.username,
            phone: user.phone,
          },
          {
            useFindAndModify: false,
            new: true
          }
        );
        req.status = 200;
        req.return = { message: 'Updated Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err);
      req.status = 404;
      req.return = { message: err.message, error: err };
      next();
    }
  },

  updateUserRole: async (req, res, next) => {
    const user = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const result = await UserModel.updateOne(
          { _id: ObjectId(user.id) },
          {
            role: user.role
          },
          {
            useFindAndModify: false,
            new: true
          }
        );
        req.status = 200;
        req.return = { message: 'Updated Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err);
      req.status = 404;
      req.return = { message: err.message, error: err };
      next();
    }
  },

  login: async (req, res, next) => {
    const user = req.body;
    try {
      const result = await UserModel.findOne({ email: user.email, password: user.password }, 'email username phone role');
      if (result !== null) {
        const token = await AuthUtil.sign({ email: result.email, role: result.role, user_id: `${result._id}` }, configAuth);
        req.status = 200;
        req.return = { user: result, token }
        next();
      } else {
        req.status = 404;
        req.return = { message: 'Email or Password invalid' }
        next();
      }
    } catch (err) {
      req.status = 404;
      req.return = err;
      next();
    }
  },
  checkAuth: async (req, res, next) => {
    try {
      const bearerToken = req.headers.authorization;
      if (!!bearerToken) {
        const Auth = AuthUtil.decode(bearerToken);
        console.log(Auth)
        req.auth = { ...Auth };
        next();
      } else {
        next();
      }
    } catch (err) {
      req.status = 404;
      req.return = err;
      next();
    }
  }
}