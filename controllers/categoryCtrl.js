const { CategoryModel } = require('../models');
const ObjectId = require("mongodb").ObjectId;

module.exports = {
  getAllCategories: async (req, res, next) => {
    const params = req.body
    const categories = await CategoryModel
      .find(params.name ? { name: { '$regex': params.name.toLowerCase(), '$options': 'i' } } : {})
      .limit(params.limit || 10)
      .skip(params.skip || 0)
    const total = await CategoryModel.countDocuments(params.name ? { name: { '$regex': params.name, '$options': 'i' } } : {});
    if (categories.length === 0) {
      req.status = 404;
      req.return = { message: "Have no Categories !" };
      next();
    }
    req.status = 200;
    req.return = { message: "Success !", data: categories, total, limit: params.limit || 10, skip: params.skip || 0 };
    next();
  },
  insertCategory: async (req, res, next) => {
    const categoryObject = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const category = new CategoryModel(categoryObject);
        const result = await category.save();
        req.status = 200;
        req.return = { message: "Insert Success !", data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Insert Failed!", error: err };
      next();
    }
  },
  updateCategory: async (req, res, next) => {
    const categoryObject = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const result = await CategoryModel.updateOne(
          { _id: ObjectId(categoryObject.id) },
          {
            name: categoryObject.name,
            description: categoryObject.description
          },
          {
            useFindAndModify: false,
            new: true
          }
        );
        req.status = 200;
        req.return = { message: 'Updated Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Update Failed!", error: err };
      next();
    }
  },
  deleteCategory: async (req, res, next) => {
    const params = req.body;
    const auth = req.auth;
    try {
      if (auth !== undefined && auth.role === 'ADMIN') {
        const result = await CategoryModel.remove({ _id: ObjectId(params.id) });
        req.status = 200;
        req.return = { message: 'Delete Success !', data: result };
        next();
      } else {
        req.status = 403;
        req.return = { message: 'Permission Dennied' };
        next();
      }
    } catch (err) {
      console.log(err)
      req.status = 404;
      req.return = { message: "Deleted Failed!", error: err };
      next();
    }
  }
}