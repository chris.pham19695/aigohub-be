const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const path = require('path');
const app = express();
const routes = require('./routes/');

//dotenv
dotenv.config();
app.use(cors());
// //Mongoose
mongoose.Promise = global.Promise;
mongoose.connect(
  `mongodb://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_NAME}`,
  // `${process.env.MONGO_ADDRESS}`,
  {
    useNewUrlParser: true,
    // useUnifiedTopology: true,
    auto_reconnect: true,
  }
);
mongoose.connection.on('error', (err) => {
  console.log('MongoDB Disconnected, Check some config from DB');
});
mongoose.connection.on('connected', (err, res) => {
  console.log('MongoDB Connected');
});

//body parser
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));

app.use('/api', routes);
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.use((req, res) => {
  return res.status(404).json({ error: 'Page not found' });
});


module.exports = app;