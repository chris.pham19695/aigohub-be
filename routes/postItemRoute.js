const express = require('express');
const router = express.Router();
const completeRes = require('../middlewares/completeRes');
const postItemCtrl = require('../controllers/postItemCtrl');
const userCtrl = require('../controllers/userCtrl');
const { PostItemValidator,
  GetPostsValidator,
  DeletePostItemValidator,
  UpdatePostItemValidator,
  GetPostValidator
} = require('../utils/validator');

router
  .route('/create')
  .post(
    userCtrl.checkAuth,
    PostItemValidator,
    postItemCtrl.insertPostItem,
    completeRes
  );

router
  .route('/all')
  .post(
    GetPostsValidator,
    postItemCtrl.getAllPost,
    completeRes
  );

router
  .route('/single')
  .post(
    GetPostValidator,
    postItemCtrl.getSinglePost,
    completeRes
  );

router
  .route('/update')
  .post(
    userCtrl.checkAuth,
    UpdatePostItemValidator,
    postItemCtrl.updatePost,
    completeRes
  );

router
  .route('/delete')
  .post(
    userCtrl.checkAuth,
    DeletePostItemValidator,
    postItemCtrl.deletePost,
    completeRes
  );
module.exports = router;
