const express = require('express');
const router = express.Router();
const completeRes = require('../middlewares/completeRes');
const typeSdkCtrl = require('../controllers/typeSdkCtrl');
const userCtrl = require('../controllers/userCtrl');
const { CreateTypeSDKValidator,
  UpdateTypeSDKValidator,
  DeleteTypeSDKValidator,
  GetAllStyleSDKValidator,
} = require('../utils/validator');


router
  .route('/create')
  .post(
    CreateTypeSDKValidator,
    userCtrl.checkAuth,
    typeSdkCtrl.insertTypeSDK,
    completeRes
  );

router
  .route('/all')
  .post(
    GetAllStyleSDKValidator,
    typeSdkCtrl.getAllTypeSDK,
    completeRes
  );

router
  .route('/update')
  .post(
    UpdateTypeSDKValidator,
    userCtrl.checkAuth,
    typeSdkCtrl.updateTypeSDK,
    completeRes
  );

router
  .route('/delete')
  .post(
    DeleteTypeSDKValidator,
    userCtrl.checkAuth,
    typeSdkCtrl.deleteTypeSDK,
    completeRes
  );
module.exports = router;
