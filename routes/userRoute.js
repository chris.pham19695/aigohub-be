const express = require('express');
const router = express.Router();
const completeRes = require('../middlewares/completeRes');
const userCtrl = require('../controllers/userCtrl');
const {
  CreateUserValidator,
  LoginUserValidator,
  GetUsersValidator,
  GetUserValidator,
  UpdateUserValidator,
  UpdateUserRoleValidator
} = require('../utils/validator');


router
  .route('/create')
  .post(
    CreateUserValidator,
    userCtrl.insertUser,
    completeRes
  );

router
  .route('/update')
  .post(
    UpdateUserValidator,
    userCtrl.checkAuth,
    userCtrl.insertUser,
    completeRes
  );

router
  .route('/updateRole')
  .post(
    UpdateUserRoleValidator,
    userCtrl.checkAuth,
    userCtrl.updateUserRole,
    completeRes
  );

router
  .route('/all')
  .post(
    GetUsersValidator,
    userCtrl.getAllUser,
    completeRes
  )

router
  .route('/single')
  .post(
    GetUserValidator,
    userCtrl.getSingleUser,
    completeRes
  )

router
  .route('/login')
  .post(
    LoginUserValidator,
    userCtrl.login,
    completeRes
  );

module.exports = router;
