const express = require('express');
const router = express.Router();


router.use('/categories', require('./categoryRoute.js'));
router.use('/posts', require('./postItemRoute'));
router.use('/users', require('./userRoute'));
router.use('/payItem', require('./payItemRoute'));
router.use('/typeSDK', require('./typeSdkRoute'));
router.use('/typeSDKUser', require('./typeSDK_UserRouter'));


module.exports = router;
