const express = require('express');
const router = express.Router();
const completeRes = require('../middlewares/completeRes');
const TypeSDKUserCtrl = require('../controllers/typeSdkUserCtrl');
const userCtrl = require('../controllers/userCtrl');
const { CreateTypeSDKUserValidator,
  UpdateTypeSDKUserValidator,
  DeleteTypeSDKUserValidator,
  GetAllStyleSDKUserValidator,
} = require('../utils/validator');


router
  .route('/create')
  .post(
    CreateTypeSDKUserValidator,
    userCtrl.checkAuth,
    TypeSDKUserCtrl.insertTypeSDKUser,
    completeRes
  );

router
  .route('/allByUser')
  .post(
    GetAllStyleSDKUserValidator,
    TypeSDKUserCtrl.getAllTypeSDKUser,
    completeRes
  );

router
  .route('/update')
  .post(
    UpdateTypeSDKUserValidator,
    userCtrl.checkAuth,
    TypeSDKUserCtrl.updateTypeSDKUser,
    completeRes
  );

router
  .route('/delete')
  .post(
    DeleteTypeSDKUserValidator,
    userCtrl.checkAuth,
    TypeSDKUserCtrl.deleteTypeSDKUser,
    completeRes
  );
module.exports = router;
