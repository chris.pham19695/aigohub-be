const express = require('express');
const router = express.Router();
const completeRes = require('../middlewares/completeRes');
const categoryCtrl = require('../controllers/categoryCtrl');
const userCtrl = require('../controllers/userCtrl');
const { CategoryValidator,
  UpdateCategoryValidator,
  DeleteCategoryValidator,
  GetCategoriesValidator
} = require('../utils/validator');


router
  .route('/create')
  .post(
    userCtrl.checkAuth,
    CategoryValidator,
    categoryCtrl.insertCategory,
    completeRes
  );

router
  .route('/all')
  .post(
    GetCategoriesValidator,
    categoryCtrl.getAllCategories,
    completeRes
  );

router
  .route('/update')
  .post(
    userCtrl.checkAuth,
    UpdateCategoryValidator,
    categoryCtrl.updateCategory,
    completeRes
  );

router
  .route('/delete')
  .post(
    userCtrl.checkAuth,
    DeleteCategoryValidator,
    categoryCtrl.deleteCategory,
    completeRes
  );
module.exports = router;
