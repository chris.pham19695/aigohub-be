const express = require('express');
const router = express.Router();
const completeRes = require('../middlewares/completeRes');
const payItemCtrl = require('../controllers/payItemCtrl');
const userCtrl = require('../controllers/userCtrl');
const { GetAllPayItemValidator,
  GetSinglePayItemValidator,
  CreatePayItemValidator,
  UpdatePayItemValidator,
  DeletePayItemValidator,
  GetPayItemByUserValidator
} = require('../utils/validator');

router
  .route('/create')
  .post(
    CreatePayItemValidator,
    userCtrl.checkAuth,
    payItemCtrl.insertPayItem,
    completeRes
  );

router
  .route('/all')
  .post(
    GetAllPayItemValidator,
    userCtrl.checkAuth,
    payItemCtrl.getAllPayItem,
    completeRes
  );

router
  .route('/allPayUser')
  .post(
    GetPayItemByUserValidator,
    userCtrl.checkAuth,
    payItemCtrl.getAllPayItemByUser,
    completeRes
  );

router
  .route('/single')
  .post(
    GetSinglePayItemValidator,
    userCtrl.checkAuth,
    payItemCtrl.getSinglePayItem,
    completeRes
  );

router
  .route('/update')
  .post(
    UpdatePayItemValidator,
    userCtrl.checkAuth,
    payItemCtrl.updatePayItem,
    completeRes
  );

router
  .route('/delete')
  .post(
    DeletePayItemValidator,
    userCtrl.checkAuth,
    payItemCtrl.deletePayItem,
    completeRes
  );
module.exports = router;
