FROM node:latest

WORKDIR /usr/src/app

COPY . .

COPY package*.json ./

RUN npm install

COPY --chown=node:node . .

EXPOSE 8080

CMD [ "npm", "start" ]
