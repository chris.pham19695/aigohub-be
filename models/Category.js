const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema({
  name: String,
  description: String,
});

CategorySchema.pre('save', (next) => {
  next();
});

module.exports = mongoose.model('Category', CategorySchema);