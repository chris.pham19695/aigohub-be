
const mongoose = require('mongoose');

const RoleSchema = new mongoose.Schema({
  role: String,
  description: String,
});

RoleSchema.pre('save', (next) => {
  next();
});

module.exports = mongoose.model('Role', RoleSchema);