const mongoose = require('mongoose');

const TypeSDK_Schema = new mongoose.Schema({
  description: String,
});

TypeSDK_Schema.pre('save', (next) => {
  next();
});

module.exports = mongoose.model('TypeSDK', TypeSDK_Schema);