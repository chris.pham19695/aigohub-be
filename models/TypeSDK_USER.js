const mongoose = require('mongoose');

const TypeSDK_User_Schema = new mongoose.Schema({
  user_id: String,
  sdk_id: String,
  key: String,
  description: String,
  create_date: Date,
  expired_date: Date,
});

TypeSDK_User_Schema.pre('save', (next) => {
  next();
});

module.exports = mongoose.model('TypeSDK_User', TypeSDK_User_Schema);