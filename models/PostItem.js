const mongoose = require('mongoose');

const Post_Item_Schema = new mongoose.Schema({
  name: String,
  description: String,
  category_id: { type: String, required: true, trim: true },
  user_id: { type: String, required: true, trim: true },
  date: Date
});

Post_Item_Schema.pre('save', (next) => {
  const now = new Date();
  if (!this.date) {
    this.date = now
  }
  next()
});

module.exports = mongoose.model('PostItem', Post_Item_Schema);