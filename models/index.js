const CategoryModel = require('./Category');
const PostItemModel = require('./PostItem');
const UserModel = require('./User');
const PayItemModel = require('./PayItem');
const RoleModel = require('./Role');
const TypeSDKModel = require('./TypeSDK');
const TypeSDK_UserModel = require('./TypeSDK_USER');

module.exports = {
  CategoryModel,
  PostItemModel,
  UserModel,
  RoleModel,
  PayItemModel,
  TypeSDK_UserModel,
  TypeSDKModel
}