const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  email: { type: String, unique: true, required: true, trim: true, index: true },
  password: String,
  username: String,
  phone: String,
  tokenLogin: String,
  role: {
    type: String,
    enum: ['USER', 'ADMIN', 'TRIAL'],
    default: 'USER',
  },
});

UserSchema.pre('save', (next) => {
  next();
});

module.exports = mongoose.model('User', UserSchema);