const mongoose = require('mongoose');

const Pay_Item_Schema = new mongoose.Schema({
  description: String,
  user_id: String,
  date_of_pay: Date
});

Pay_Item_Schema.pre('save', (next) => {
  const now = new Date();
  if (!this.date_of_pay) {
    this.date_of_pay = now
  }
  next()
});

module.exports = mongoose.model('PayItem', Pay_Item_Schema);