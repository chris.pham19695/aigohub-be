'use strict';
// IMPORTS
let shortid = require('shortid');

shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_@');
//shortid.characters('abc');

module.exports = shortid;
