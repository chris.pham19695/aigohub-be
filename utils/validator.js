const Joi = require('joi');

exports.CategoryValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}
exports.GetCategoriesValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    name: Joi.string(),
    skip: Joi.number(),
    limit: Joi.number()
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.GetPostsValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    name: Joi.string(),
    skip: Joi.number(),
    limit: Joi.number()
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.UpdateCategoryValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().required(),
    id: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.DeleteCategoryValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    id: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.DeletePostValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    id: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.UpdatePostItemValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().required(),
    id: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.PostItemValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().required(),
    user_id: Joi.string().required(),
    category_id: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.DeletePostItemValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    id: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.CreateUserValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    username: Joi.string(),
    phone: Joi.string(),
    tokenLogin: Joi.string(),
    role: Joi.string().valid('ADMIN', 'USER', 'TRIAL').required()
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.GetUsersValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    name: Joi.string(),
    email: Joi.string(),
    skip: Joi.number(),
    limit: Joi.number()
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.GetUserValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    id: Joi.string().required()
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.GetPostValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    id: Joi.string().required()
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}


exports.LoginUserValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.DeleteUserValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    id: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.UpdateUserValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    id: Joi.string().required(),
    email: Joi.string().email(),
    password: Joi.string(),
    username: Joi.string(),
    phone: Joi.string(),
    role: Joi.string().valid('admin', 'user', 'trial')
  });

  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.UpdateUserRoleValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    id: Joi.string().required(),
    role: Joi.string().valid('ADMIN', 'USER', 'TRIAL').required()
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.GetAllPayItemValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    skip: Joi.number(),
    limit: Joi.number(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.GetSinglePayItemValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    pay_id: Joi.string().required()
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.GetPayItemByUserValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    skip: Joi.number(),
    limit: Joi.number()
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.CreatePayItemValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    description: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.UpdatePayItemValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    pay_id: Joi.string().required(),
    description: Joi.string().required(),
    date_of_pay: Joi.date().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.DeletePayItemValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    pay_id: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.GetAllStyleSDKValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({});
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.CreateTypeSDKValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    description: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.UpdateTypeSDKValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    sdk_id: Joi.string().required(),
    description: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.DeleteTypeSDKValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    sdk_id: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.GetAllStyleSDKUserValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    skip: Joi.number(),
    limit: Joi.number()
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.CreateTypeSDKUserValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    sdk_id: Joi.string().required(),
    description: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.UpdateTypeSDKUserValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    sdk_id: Joi.string().required(),
    description: Joi.string().required(),
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}

exports.DeleteTypeSDKUserValidator = async (req, res, next) => {
  const data = req.body;
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    sdk_id: Joi.string().required()
  });
  const { error } = await schema.validate(data);
  if (error) {
    console.log(error.message);
    res.status(422).json({
      status: 'error',
      message: error.message,
      err: error.message,
    })
  } else {
    next();
  }
}






